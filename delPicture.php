<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);

$key= $_POST['key'];
$deleteID = $_POST['deleteID'];

//Check key
$result = $db->select("user","*",[
    "hashkey"=>$key
]);
if(sizeof($result)== 0){
    echo "logout";
} else {
    $target = "picture/" . $deleteID . ".jpg";
    unlink ($target);
    $db->delete("picture",[
        "picID"=>$deleteID
    ]);
    echo "finish";
}

?>