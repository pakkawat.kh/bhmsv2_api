<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$frontCover = $_POST['frontCover'];
$info = $_POST['info'];
$overviewChart = $_POST['overviewChart'];
$chart = $_POST['chart'];
$event = $_POST['event'];
$conclusion = $_POST['conclusion'];
$backCover = $_POST['backCover'];
$projectID = $_POST['projectID'];

$db->delete("onscreenreport",[
    "projectID"=>$projectID
]);

$db->insert("onscreenreport",[
    "frontCover"=>$frontCover,
    "info"=>$info,
    "overviewChart"=>$overviewChart,
    "chart"=>$chart,
    "event"=>$event,
    "conclusion"=>$conclusion,
    "backCover"=>$backCover,
    "projectID"=>$projectID
]);