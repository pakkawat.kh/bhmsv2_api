-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 11, 2022 at 12:44 AM
-- Server version: 8.0.17
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bhmsv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `autoreport`
--

CREATE TABLE `autoreport` (
  `projectID` int(11) NOT NULL,
  `frontCover` int(11) NOT NULL,
  `info` int(11) NOT NULL,
  `overviewchart` int(11) NOT NULL,
  `chart` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `conclusion` int(11) NOT NULL,
  `backCover` int(11) NOT NULL,
  `daily` int(11) NOT NULL,
  `weekly` int(11) NOT NULL,
  `monthly` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `autoreport`
--

INSERT INTO `autoreport` (`projectID`, `frontCover`, `info`, `overviewchart`, `chart`, `event`, `conclusion`, `backCover`, `daily`, `weekly`, `monthly`) VALUES
(3, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `onscreenreport`
--

CREATE TABLE `onscreenreport` (
  `projectID` int(11) NOT NULL,
  `frontCover` int(11) NOT NULL,
  `info` int(11) NOT NULL,
  `overviewchart` int(11) NOT NULL,
  `chart` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `conclusion` int(11) NOT NULL,
  `backCover` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `onscreenreport`
--

INSERT INTO `onscreenreport` (`projectID`, `frontCover`, `info`, `overviewchart`, `chart`, `event`, `conclusion`, `backCover`) VALUES
(3, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE `picture` (
  `picID` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `label` text NOT NULL,
  `fileName` text NOT NULL,
  `projectID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `picture`
--

INSERT INTO `picture` (`picID`, `orderID`, `label`, `fileName`, `projectID`) VALUES
(12, 100, 'tesr1', 'kateryna-hliznitsova-8nIKrlbnGPM-unsplash.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `projectID` int(11) NOT NULL,
  `projectName` text NOT NULL,
  `isPassword` tinyint(4) NOT NULL,
  `password` text NOT NULL,
  `shortenURL` text NOT NULL,
  `address` text NOT NULL,
  `dateTime` text NOT NULL,
  `duration` int(11) NOT NULL,
  `linenumber` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`projectID`, `projectName`, `isPassword`, `password`, `shortenURL`, `address`, `dateTime`, `duration`, `linenumber`, `status`) VALUES
(3, 'Param 5 bridge', 1, '1234567', 'param5', 'Param 5 bridge, Nontraburi', '2022-09-03 12:00', 3000, 12, 1),
(4, 'Param 8 bridge', 0, '', 'param8', 'param 8 bridge, Nontraburi', '2022-09-09 1:00', 5000, 12, 1),
(5, 'Param 9 Bridge', 1, '1234578', 'Param9', 'Param 9 Bridge, Bangkok', '2022-09-05 12:00', 5000, 17, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `adminID` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `hashkey` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`adminID`, `username`, `password`, `hashkey`) VALUES
(1, 'admin', '1234', '8aN^XCimZEdA7yVS');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autoreport`
--
ALTER TABLE `autoreport`
  ADD PRIMARY KEY (`projectID`);

--
-- Indexes for table `onscreenreport`
--
ALTER TABLE `onscreenreport`
  ADD PRIMARY KEY (`projectID`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`picID`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`projectID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`adminID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
  MODIFY `picID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `projectID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
