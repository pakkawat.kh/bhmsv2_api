<?php
require_once('connection.php');
$key = $_POST['key'];
$orderID = $_POST['orderID'];
$label= $_POST['label'];
$fileName = $_POST['fileName'];
$fileData = $_POST['fileData'];
$projectID = $_POST['projectID'];
$result = $db->select("user","*",[
    "hashkey"=>$key
]);
if(sizeof($result)== 0){
    echo "logout";
} else {
    //Add to picture table
    $db->insert("picture",[
        "orderID"=>$orderID,
        "label"=>$label,
        "fileName"=>$fileName,
        "projectID"=>$projectID
    ]);
    //add picture
    $picID = $db->id();
    $target = "picture/" . $picID . ".jpg";
    unlink ($target);
    move_uploaded_file($_FILES['fileData']['tmp_name'],$target);
    echo "finish";
}

?>