<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$projectID = $_POST['projectID'];
$projectName = $_POST['projectName'];
$isPassword = $_POST['isPassword'];
$password = $_POST['password'];
$shortenURL = $_POST['shortenURL'];
$address = $_POST['address'];
$dateTime = $_POST['dateTime'];
$duration = $_POST['duration'];
$linenumber = $_POST['linenumber'];
$key = $_POST['key'];
//Check key
$result = $db->select("user","*",[
    "hashkey"=>$key
]);

if(sizeof($result)== 0){
    echo "logout";
} else {

    $db->update("project",[
        "projectName"=>$projectName,
        "isPassword"=>$isPassword,
        "password"=>$password,
        "shortenURL"=>$shortenURL,
        "address"=>$address,
        "dateTime"=>$dateTime,
        "duration"=>$duration,
        "linenumber"=>$linenumber    
    ],[
        "projectID"=>$projectID
    ]);
    echo "finish";
}
?>